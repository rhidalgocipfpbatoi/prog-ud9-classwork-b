package actividad8;

public class WaitApp {

    public static void main(String[] args) {
        try {
            Thread.currentThread().interrupt();
            waitSeconds(20);
            System.out.println("Fin");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    private static void waitSeconds(int seconds) throws InterruptedException{
        Thread.sleep(seconds * 1000);
    }
}
