package actividad13;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Actividad13 {
    public static void main(String[] args) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        System.out.println("Introduce una fecha en formato (YYYY/MM/DD hh:mm:ss)");
        Scanner scanner = new Scanner(System.in);
        String dateTime = scanner.nextLine();

        LocalDateTime localDateTime = LocalDateTime.parse(dateTime, inputFormatter);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        String localDateTimeString = localDateTime.format(outputFormatter);

        System.out.println(localDateTimeString);


    }
}
