package actividad1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad1 {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int maximo = Integer.MIN_VALUE;
        for (int i = 0; i < 6; i++) {
            try {
                int numero = scanner.nextInt();
                if (numero > maximo) {
                    maximo = numero;
                }
            } catch (InputMismatchException e) {
                System.out.println("Se ha introducido un dato erróneo");
                i--;
                scanner.next();
            }
        }

        System.out.printf("El máximo es %d", maximo);
    }


}
