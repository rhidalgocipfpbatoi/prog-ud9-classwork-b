package actividad11;


import actividad11.excepcion.SaldoAgotadoExcepcion;

public class BonoAutobus {

    private final int DEFAULT_SALDO = 10;

    private int saldo;

    public BonoAutobus(){
        saldo = DEFAULT_SALDO;
    }

    public void fichar() throws SaldoAgotadoExcepcion {
        if (saldo == 0) {
            throw new SaldoAgotadoExcepcion();
        }
        saldo--;
    }

}
