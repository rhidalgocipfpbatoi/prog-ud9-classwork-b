package actividad3;

import java.util.InputMismatchException;
import java.util.Scanner;
public class Actividad3 {

    public static void main(String[] args) {

        System.out.println("Num Introducidos: " + getNumbers());
    }

    public static String getNumbers() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder stringBuilder = new StringBuilder();
        do {
            System.out.println("Introduce un número");
            try {
                int number = scanner.nextInt();
                if (number >= 1 && number <= 5 ){
                    return stringBuilder.toString();
                }
                stringBuilder.append("," + number);
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un entero");
                scanner.nextLine();
            }
        } while (true);
    }
}
