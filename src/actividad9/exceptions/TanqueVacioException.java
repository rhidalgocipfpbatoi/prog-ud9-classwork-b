package actividad9.exceptions;

public class TanqueVacioException extends Exception {
    public TanqueVacioException(String message) {
        super(message);
    }
}
